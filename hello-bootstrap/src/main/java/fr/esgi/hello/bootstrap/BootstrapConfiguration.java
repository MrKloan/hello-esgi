package fr.esgi.hello.bootstrap;

import org.update4j.Configuration;
import org.update4j.FileMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

public class BootstrapConfiguration {

    public static void main(final String[] args) throws IOException {
        final String applicationVersion = args[0];
        final String buildDirectory = args[1];

        final Configuration configuration = Configuration.builder()
                .baseUri(format("https://gitlab.com/MrKloan/hello-esgi/-/jobs/%s/artifacts/raw/", System.getenv("CI_JOB_ID")))
                .basePath("${user.home}/.hello/")
                .property("default.launcher.main.class", "fr.esgi.hello.application.HelloWorld")
                .file(FileMetadata
                        .readFrom(format("hello-application/target/hello-application-%s-shaded.jar", applicationVersion))
                        .path("hello.jar")
                        .classpath()
                )
                .build();

        Files.writeString(Path.of(buildDirectory, "hello.xml"), configuration.toString());
    }
}
