# Update4J

Download the `update4j` binary from [Maven Central](https://search.maven.org/search?q=g:org.update4j), then run the 
following command: 
```sh
java -jar update4j-{version}.jar --remote https://gitlab.com/MrKloan/hello-esgi/-/jobs/artifacts/master/raw/hello.xml?job=build
```

You can also run a local configuration using:
```sh
java -jar update4j-{version}.jar --local path/to/configuration.xml
```

Sample configuration file:
```xml
<?xml version="1.1" encoding="UTF-8" standalone="yes"?>
<configuration timestamp="1970-01-01T00:00:00.000Z">
    <base uri="https://example.com/download/" path="${user.home}/.app/"/>
    <properties>
        <property key="default.launcher.main.class" value="com.example.Application"/>
    </properties>
    <files>
        <file path="app.jar" size="42" checksum="29656c37" classpath="true"/>
    </files>
</configuration>
```